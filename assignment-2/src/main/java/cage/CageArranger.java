// Laksono Bramantio
// 1706984650

package cage;

import java.util.ArrayList;

public class CageArranger {

    public static ArrayList<Cage>[] preArrange(ArrayList<Cage> cages) {
        int cagesLength = cages.size();
        ArrayList<Cage> level3 = new ArrayList<>();
        ArrayList<Cage> level2 = new ArrayList<>();
        ArrayList<Cage> level1 = new ArrayList<>();

        // Menyusun kandang
        if (cagesLength == 1) {
            level1.add(cages.get(0));
        } else if (cagesLength == 2) {
            level1.add(cages.get(0));
            level2.add(cages.get(1));
        } else {
            for (int i = 0; i < cagesLength / 3; i++) {
                level1.add(cages.get(i));
            }
            for (int i = cagesLength / 3; i < cagesLength / 3 * 2; i++) {
                level2.add(cages.get(i));
            }
            for (int i = cagesLength / 3 * 2; i < cagesLength; i++) {
                level3.add(cages.get(i));
            }
        }
        ArrayList<Cage>[] arrPreArrange = new ArrayList[]{level3, level2, level1};

        return arrPreArrange;
    }

    public static ArrayList<Cage>[] postArrange(ArrayList<Cage>[] cages) {
        for (ArrayList<Cage> rowCages : cages) { //reverse
            for (int i = 0; i < (rowCages.size() / 2); i++) {
                Cage temp = rowCages.get(i);
                rowCages.set(i, rowCages.get(rowCages.size() - 1 - i));
                rowCages.set((rowCages.size() - 1 - i), temp);
            }
        }

        ArrayList<Cage> temp = cages[0]; //shift
        cages[0] = cages[1];
        cages[1] = cages[2];
        cages[2] = temp;

        return cages;
    }

    public static void printArr(ArrayList<Cage>[] arr) {
        ArrayList<Cage> level3 = arr[0];
        ArrayList<Cage> level2 = arr[1];
        ArrayList<Cage> level1 = arr[2];

        if (level3.size() == 0 && level2.size() == 0 && level1.size() == 0) ; // tidak print
        else {

            System.out.print("level 3: ");
            for (int i = 0; i < level3.size(); i++) {
                System.out.print(level3.get(i).getInfo());
            }
            System.out.println();

            System.out.print("level 2: ");
            for (int i = 0; i < level2.size(); i++) {
                System.out.print(level2.get(i).getInfo());
            }
            System.out.println();

            System.out.print("level 1: ");
            for (int i = 0; i < level1.size(); i++) {
                System.out.print(level1.get(i).getInfo());
            }
            System.out.println("\n");
        }
    }
}
