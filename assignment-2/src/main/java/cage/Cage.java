// Laksono Bramantio
// 1706984650

package cage;

import animal.Cat;
import animal.Lion;
import animal.Eagle;
import animal.Parrot;
import animal.Hamster;
import java.util.ArrayList;

public class Cage {
    private Cat cat;
    private Lion lion;
    private Eagle eagle;
    private Parrot parrot;
    private Hamster hamster;

    private String name;
    private int length;
    private String cageType;

    // ArrayList untuk menyimpan kandang-kandang tiap hewan
    private static ArrayList<Cage> cagesOfCat = new ArrayList<>();
    private static ArrayList<Cage> cagesOfLion = new ArrayList<>();
    private static ArrayList<Cage> cagesOfEagle = new ArrayList<>();
    private static ArrayList<Cage> cagesOfParrot = new ArrayList<>();
    private static ArrayList<Cage> cagesOfHamster = new ArrayList<>();

    // Constructor Cage tiap hewan
    public Cage(Cat cat) {
        this.cat = cat;
        this.name = cat.getName();
        this.length = cat.getLength();
        this.cageType = this.setCageType(cat.getLength(), Cat.getCageLoc());
    }

    public Cage(Lion lion) {
        this.lion = lion;
        this.name = lion.getName();
        this.length = lion.getLength();
        this.cageType = this.setCageType(lion.getLength(), Lion.getCageLoc());
    }

    public Cage(Eagle eagle) {
        this.eagle = eagle;
        this.name = eagle.getName();
        this.length = eagle.getLength();
        this.cageType = this.setCageType(eagle.getLength(), Eagle.getCageLoc());
    }

    public Cage(Parrot parrot) {
        this.parrot = parrot;
        this.name = parrot.getName();
        this.length = parrot.getLength();
        this.cageType = this.setCageType(parrot.getLength(), Parrot.getCageLoc());
    }

    public Cage(Hamster hamster) {
        this.hamster = hamster;
        this.name = hamster.getName();
        this.length = hamster.getLength();
        this.cageType = this.setCageType(hamster.getLength(), Hamster.getCageLoc());
    }

    // Method getter untuk ArrayList Cages tiap hewan
    public static ArrayList<Cage> getCagesOfCat() {
        return cagesOfCat;
    }

    public static ArrayList<Cage> getCagesOfLion() {
        return cagesOfLion;
    }

    public static ArrayList<Cage> getCagesOfEagle() {
        return cagesOfEagle;
    }

    public static ArrayList<Cage> getCagesOfParrot() {
        return cagesOfParrot;
    }

    public static ArrayList<Cage> getCagesOfHamster() {
        return cagesOfHamster;
    }

    public String setCageType(int length, String cageLoc) {
        if (cageLoc.equals("indoor")) {
            if (length < 45) return "A";
            else if (length > 60) return "C";
            else return "B";
        } else {
            if (length < 75) return "A";
            else if (length > 90) return "C";
            else return "B";
        }
    }

    public String getInfo() {
        return name + " (" + length + " - " + cageType + "), ";
    }


}
