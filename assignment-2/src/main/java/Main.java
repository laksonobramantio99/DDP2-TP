// Laksono Bramantio
// 1706984650

import animal.Cat;
import animal.Lion;
import animal.Eagle;
import animal.Parrot;
import animal.Hamster;
import cage.Cage;
import cage.CageArranger;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        try { // Memasukkan ke blok try untuk menhindari error

            System.out.println("Welcome to Javari Park!\n" +
                    "Input the number of animals");

            int jumlahHewan; //deklarasi dataType untuk jumlah hewan yang akan dimasukkan
            // 1. Cat
            System.out.print("cat: ");
            jumlahHewan = Integer.parseInt(input.nextLine());
            if (jumlahHewan > 0) { //Jika ada kucing yang ingin dimasukkan
                System.out.println("Provide the information of cat(s):");
                String masukan = input.nextLine();
                if (jumlahHewan == 1) masukan += ",";

                String[] arr = masukan.split(",");
                for (String element : arr) {
                    String[] temp = element.split("\\|");
                    Cat newCat = new Cat(temp[0], Integer.parseInt(temp[1]));
                    Cat.getArrCat().add(newCat); // memasukkan ke arrayList Cat

                    Cage newCage = new Cage(newCat);
                    Cage.getCagesOfCat().add(newCage); // memasukkan ke arrayList cages of Cat
                }
            }

            // 2. Lion
            System.out.print("lion: ");
            jumlahHewan = Integer.parseInt(input.nextLine());
            if (jumlahHewan > 0) { //Jika ada singa yang ingin dimasukkan
                System.out.println("Provide the information of lion(s):");
                String masukan = input.nextLine();
                if (jumlahHewan == 1) masukan += ",";

                String[] arr = masukan.split(",");
                for (String element : arr) {
                    String[] temp = element.split("\\|");
                    Lion newLion = new Lion(temp[0], Integer.parseInt(temp[1]));
                    Lion.getArrLion().add(newLion); // memasukkan ke arrayList Lion

                    Cage newCage = new Cage(newLion);
                    Cage.getCagesOfLion().add(newCage); // memasukkan ke arrayList cages of Lion
                }
            }

            // 3. Eagle
            System.out.print("eagle: ");
            jumlahHewan = Integer.parseInt(input.nextLine());
            if (jumlahHewan > 0) { //Jika ada elang yang ingin dimasukkan
                System.out.println("Provide the information of eagle(s):");
                String masukan = input.nextLine();
                if (jumlahHewan == 1) masukan += ",";

                String[] arr = masukan.split(",");
                for (String element : arr) {
                    String[] temp = element.split("\\|");
                    Eagle newEagle = new Eagle(temp[0], Integer.parseInt(temp[1]));
                    Eagle.getArrEagle().add(newEagle); // memasukkan ke arrayList Eagle

                    Cage newCage = new Cage(newEagle);
                    Cage.getCagesOfEagle().add(newCage); // memasukkan ke arrayList cages of Eagle
                }
            }

            // 4. Parrot
            System.out.print("parrot: ");
            jumlahHewan = Integer.parseInt(input.nextLine());
            if (jumlahHewan > 0) { //Jika ada beo yang ingin dimasukkan
                System.out.println("Provide the information of parrot(s):");
                String masukan = input.nextLine();
                if (jumlahHewan == 1) masukan += ",";

                String[] arr = masukan.split(",");
                for (String element : arr) {
                    String[] temp = element.split("\\|");
                    Parrot newParrot = new Parrot(temp[0], Integer.parseInt(temp[1]));
                    Parrot.getArrParrot().add(newParrot); // memasukkan ke arrayList Parrot

                    Cage newCage = new Cage(newParrot);
                    Cage.getCagesOfParrot().add(newCage); // memasukkan ke arrayList cages of Parrot
                }
            }

            // 5. Hamster
            System.out.print("hamster: ");
            jumlahHewan = Integer.parseInt(input.nextLine());
            if (jumlahHewan > 0) { //Jika ada beo yang ingin dimasukkan
                System.out.println("Provide the information of hamster(s):");
                String masukan = input.nextLine();
                if (jumlahHewan == 1) masukan += ",";

                String[] arr = masukan.split(",");
                for (String element : arr) {
                    String[] temp = element.split("\\|");
                    Hamster newHamster = new Hamster(temp[0], Integer.parseInt(temp[1]));
                    Hamster.getArrHamster().add(newHamster); // memasukkan ke arrayList Hamster

                    Cage newCage = new Cage(newHamster);
                    Cage.getCagesOfHamster().add(newCage); // memasukkan ke arrayList cages of Hamster
                }
            }

            System.out.println("Animals have been successfully recorded!\n" +
                    "\n" +
                    "=============================================");

            // Tahap Arrangement
            System.out.println("Cage arrangement:");

            // 1. Cat
            if (Cat.getArrCat().size() != 0) {
                System.out.println("location: indoor"); // pre arrangement
            }
            Cat.setPreArrangeCatArr(CageArranger.preArrange(Cage.getCagesOfCat()));
            CageArranger.printArr(Cat.getPreArrangeCatArr());

            if (Cat.getArrCat().size() != 0) {
                System.out.println("After rearrangement..."); // post arrangement
            }
            Cat.setPostArrangeCatArr(CageArranger.postArrange(Cat.getPreArrangeCatArr()));
            CageArranger.printArr(Cat.getPostArrangeCatArr());

            // 2. Lion
            if (Lion.getArrLion().size() != 0) {
                System.out.println("location: outdoor"); // pre arrangement
            }
            Lion.setPreArrangeLionArr(CageArranger.preArrange(Cage.getCagesOfLion()));
            CageArranger.printArr(Lion.getPreArrangeLionArr());

            if (Lion.getArrLion().size() != 0) {
                System.out.println("After rearrangement..."); // post arrangement
            }
            Lion.setPostArrangeLionArr(CageArranger.postArrange(Lion.getPreArrangeLionArr()));
            CageArranger.printArr(Lion.getPostArrangeLionArr());

            // 3. Eagle
            if (Eagle.getArrEagle().size() != 0) {
                System.out.println("location: outdoor"); // pre arrangement
            }
            Eagle.setPreArrangeEagleArr(CageArranger.preArrange(Cage.getCagesOfEagle()));
            CageArranger.printArr(Eagle.getPreArrangeEagleArr());

            if (Eagle.getArrEagle().size() != 0) {
                System.out.println("After rearrangement..."); // post arrangement
            }
            Eagle.setPostArrangeEagleArr(CageArranger.postArrange(Eagle.getPreArrangeEagleArr()));
            CageArranger.printArr(Eagle.getPostArrangeEagleArr());

            // 4. Parrot
            if (Parrot.getArrParrot().size() != 0) {
                System.out.println("location: indoor"); // pre arrangement
            }
            Parrot.setPreArrangeParrotArr(CageArranger.preArrange(Cage.getCagesOfParrot()));
            CageArranger.printArr(Parrot.getPreArrangeParrotArr());

            if (Parrot.getArrParrot().size() != 0) {
                System.out.println("After rearrangement..."); // post arrangement
            }
            Parrot.setPostArrangeParrotArr(CageArranger.postArrange(Parrot.getPreArrangeParrotArr()));
            CageArranger.printArr(Parrot.getPostArrangeParrotArr());

            // 5. Hamster
            if (Hamster.getArrHamster().size() != 0) {
                System.out.println("location: indoor"); // pre arrangement
            }
            Hamster.setPreArrangeHamsterArr(CageArranger.preArrange(Cage.getCagesOfHamster()));
            CageArranger.printArr(Hamster.getPreArrangeHamsterArr());

            if (Hamster.getArrHamster().size() != 0) {
                System.out.println("After rearrangement..."); // post arrangement
            }
            Hamster.setPostArrangeHamsterArr(CageArranger.postArrange(Hamster.getPreArrangeHamsterArr()));
            CageArranger.printArr(Hamster.getPostArrangeHamsterArr());

            // Number of Animals
            System.out.println("NUMBER OF ANIMALS:");
            System.out.println("cat:" + Cat.getArrCat().size());
            System.out.println("lion:" + Lion.getArrLion().size());
            System.out.println("parrot:" + Parrot.getArrParrot().size());
            System.out.println("eagle:" + Eagle.getArrEagle().size());
            System.out.println("hamster:" + Hamster.getArrHamster().size());

            System.out.println("\n=============================================");

            while (true) {
                System.out.println("Which animal you want to visit?\n" +
                        "(1: Cat, 2: Eagle, 3: Hamster, 4: Parrot, 5: Lion, 99: Exit)");

                String keyword = input.nextLine();

                if (keyword.equals("1")) {
                    Cat.visitCat();
                } else if (keyword.equals("2")) {
                    Eagle.visitEagle();
                } else if (keyword.equals("3")) {
                    Hamster.visitHamster();
                } else if (keyword.equals("4")) {
                    Parrot.visitParrot();
                } else if (keyword.equals("5")) {
                    Lion.visitLion();
                } else if (keyword.equals("99")) {
                    System.out.println("Bye...");
                    break;
                } else {
                    System.out.println("Invalid input!");
                }
            }
        } catch (Exception e) { // Exception
            System.out.println("Invalid input!");
            System.exit(0);
        }
    }
}
