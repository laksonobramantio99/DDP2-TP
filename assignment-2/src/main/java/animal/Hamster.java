// Laksono Bramantio
// 1706984650

package animal;

import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;

public class Hamster extends Animal {
    private static Scanner input = new Scanner(System.in);

    private String name;
    private int length;

    private static final String cageLoc = "indoor";
    private static ArrayList<Hamster> arrHamster = new ArrayList<>();
    private static ArrayList<Cage>[] preArrangeHamsterArr;
    private static ArrayList<Cage>[] postArrangeHamsterArr;

    public Hamster(String name, int length) {
        super(name, length);
    }

    public static ArrayList<Hamster> getArrHamster() {
        return arrHamster;
    }

    public static String getCageLoc() {
        return cageLoc;
    }

    public static ArrayList<Cage>[] getPreArrangeHamsterArr() {
        return preArrangeHamsterArr;
    }

    public static void setPreArrangeHamsterArr(ArrayList<Cage>[] preArrangeHamsterArr) {
        Hamster.preArrangeHamsterArr = preArrangeHamsterArr;
    }

    public static ArrayList<Cage>[] getPostArrangeHamsterArr() {
        return postArrangeHamsterArr;
    }

    public static void setPostArrangeHamsterArr(ArrayList<Cage>[] postArrangeHamsterArr) {
        Hamster.postArrangeHamsterArr = postArrangeHamsterArr;
    }

    public static void visitHamster() {
        System.out.print("Mention the name of hamster you want to visit: ");
        String hamsterName = input.nextLine();

        if (!isTheHamsterAvailable(hamsterName)) {
            System.out.println("There is no hamster with that name! Back to the office!\n");
        } else {
            System.out.println("You are visiting " + hamsterName + " (hamster) now, what would you like to do?\n" +
                    "1: See it gnawing 2: Order to run in the hamster wheel");
            String command = input.nextLine();

            if (command.equals("1")) { //See it gnawing
                System.out.println(hamsterName + " makes a voice: ngkkrit.. ngkkrrriiit\n" +
                        "Back to the office!\n");
            } else if (command.equals("2")) { //Order to run in the hamster wheel
                System.out.println(hamsterName + " makes a voice: trrr…. trrr...\n" +
                        "Back to the office!\n");
            } else {
                System.out.println("You do nothing...\n" +
                        "Back to the office!\n");
            }
        }
    }

    public static boolean isTheHamsterAvailable(String hamsterName) {
        boolean isAvailable = false;
        for (Hamster hamster : arrHamster) {
            if (hamster.getName().equals(hamsterName)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }
}


