// Laksono Bramantio
// 1706984650

package animal;

import cage.Cage;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Cat extends Animal {
    private static Scanner input = new Scanner(System.in);

    private String name;
    private int length;

    private static final String cageLoc = "indoor";
    private static ArrayList<Cat> arrCat = new ArrayList<>();
    private static ArrayList<Cage>[] preArrangeCatArr;
    private static ArrayList<Cage>[] postArrangeCatArr;

    public Cat(String name, int length) {
        super(name, length);
    }

    public static ArrayList<Cat> getArrCat() {
        return arrCat;
    }

    public static String getCageLoc() {
        return cageLoc;
    }

    public static ArrayList<Cage>[] getPreArrangeCatArr() {
        return preArrangeCatArr;
    }

    public static void setPreArrangeCatArr(ArrayList<Cage>[] preArrangeCatArr) {
        Cat.preArrangeCatArr = preArrangeCatArr;
    }

    public static ArrayList<Cage>[] getPostArrangeCatArr() {
        return postArrangeCatArr;
    }

    public static void setPostArrangeCatArr(ArrayList<Cage>[] postArrangeCatArr) {
        Cat.postArrangeCatArr = postArrangeCatArr;
    }

    public static void visitCat() {
        System.out.print("Mention the name of cat you want to visit: ");
        String catName = input.nextLine();

        if (!isTheCatAvailable(catName)) {
            System.out.println("There is no cat with that name! Back to the office!\n");
        } else {
            System.out.println("You are visiting " + catName + " (cat) now, what would you like to do?\n" +
                    "1: Brush the fur 2: Cuddle");
            String command = input.nextLine();

            if (command.equals("1")) { //Brush the fur
                System.out.println("Time to clean " + catName + "'s fur\n" +
                        catName + " makes a voice: Nyaaan...\n" +
                        "Back to the office!\n");
            } else if (command.equals("2")) { //Cuddle
                Random rand = new Random();
                int index = rand.nextInt(4); //untuk random suara kucing
                String[] arrCatVoice = new String[]{"Miaaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};

                String catVoice = arrCatVoice[index];

                System.out.println(catName + " makes a voice: " + catVoice + "\n" +
                        "Back to the office!\n");

            } else {
                System.out.println("You do nothing...\n" +
                        "Back to the office!\n");
            }
        }
    }

    public static boolean isTheCatAvailable(String catName) {
        boolean isAvailable = false;
        for (Cat cat : arrCat) {
            if (cat.getName().equals(catName)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }
}