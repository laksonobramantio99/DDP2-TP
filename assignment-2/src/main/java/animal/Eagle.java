// Laksono Bramantio
// 1706984650

package animal;

import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;

public class Eagle extends Animal {
    private static Scanner input = new Scanner(System.in);

    private String name;
    private int length;

    private static final String cageLoc = "outdoor";
    private static ArrayList<Eagle> arrEagle = new ArrayList<>();
    private static ArrayList<Cage>[] preArrangeEagleArr;
    private static ArrayList<Cage>[] postArrangeEagleArr;

    public Eagle(String name, int length) {
        super(name, length);
    }

    public static ArrayList<Eagle> getArrEagle() {
        return arrEagle;
    }

    public static String getCageLoc() {
        return cageLoc;
    }

    public static ArrayList<Cage>[] getPreArrangeEagleArr() {
        return preArrangeEagleArr;
    }

    public static void setPreArrangeEagleArr(ArrayList<Cage>[] preArrangeEagleArr) {
        Eagle.preArrangeEagleArr = preArrangeEagleArr;
    }

    public static ArrayList<Cage>[] getPostArrangeEagleArr() {
        return postArrangeEagleArr;
    }

    public static void setPostArrangeEagleArr(ArrayList<Cage>[] postArrangeEagleArr) {
        Eagle.postArrangeEagleArr = postArrangeEagleArr;
    }

    public static void visitEagle() {
        System.out.print("Mention the name of eagle you want to visit: ");
        String eagleName = input.nextLine();

        if (!isTheEagleAvailable(eagleName)) {
            System.out.println("There is no eagle with that name! Back to the office!\n");
        } else {
            System.out.println("You are visiting " + eagleName + " (eagle) now, what would you like to do?\n" +
                    "1: Order to fly");
            String command = input.nextLine();

            if (command.equals("1")) { //Order to fly
                System.out.println(eagleName + " makes a voice: kwaakk…\n" +
                        "You hurt!\n" +
                        "Back to the office!\n");
            } else {
                System.out.println("You do nothing...\n" +
                        "Back to the office!\n");
            }
        }
    }

    public static boolean isTheEagleAvailable(String eagleName) {
        boolean isAvailable = false;
        for (Eagle eagle : arrEagle) {
            if (eagle.getName().equals(eagleName)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }
}
