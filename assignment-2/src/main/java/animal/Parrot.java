// Laksono Bramantio
// 1706984650

package animal;

import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;

public class Parrot extends Animal {
    private static Scanner input = new Scanner(System.in);

    private String name;
    private int length;

    private static final String cageLoc = "indoor";
    private static ArrayList<Parrot> arrParrot = new ArrayList<>();
    private static ArrayList<Cage>[] preArrangeParrotArr;
    private static ArrayList<Cage>[] postArrangeParrotArr;

    public Parrot(String name, int length) {
        super(name, length);
    }

    public static ArrayList<Parrot> getArrParrot() {
        return arrParrot;
    }

    public static String getCageLoc() {
        return cageLoc;
    }

    public static ArrayList<Cage>[] getPreArrangeParrotArr() {
        return preArrangeParrotArr;
    }

    public static void setPreArrangeParrotArr(ArrayList<Cage>[] preArrangeParrotArr) {
        Parrot.preArrangeParrotArr = preArrangeParrotArr;
    }

    public static ArrayList<Cage>[] getPostArrangeParrotArr() {
        return postArrangeParrotArr;
    }

    public static void setPostArrangeParrotArr(ArrayList<Cage>[] postArrangeParrotArr) {
        Parrot.postArrangeParrotArr = postArrangeParrotArr;
    }

    public static void visitParrot() {
        System.out.print("Mention the name of parrot you want to visit: ");
        String parrotName = input.nextLine();

        if (!isTheParrotAvailable(parrotName)) {
            System.out.println("There is no parrot with that name! Back to the office!\n");
        } else {
            System.out.println("You are visiting " + parrotName + " (parrot) now, what would you like to do?\n" +
                    "1: Order to fly 2: Do conversation");
            String command = input.nextLine();

            if (command.equals("1")) { //Order to fly
                System.out.println("Parrot " + parrotName + " flies!\n" +
                        parrotName + " makes a voice: FLYYYY…..\n" +
                        "Back to the office!\n");
            } else if (command.equals("2")) { //Do conversation
                System.out.print("You say: ");
                String text = input.nextLine();
                System.out.println("\n" + parrotName + " says: " + text.toUpperCase());
                System.out.println("Back to the office!\n");
            } else {
                System.out.println(parrotName + " says: HM?\n" +
                        "Back to the office!\n");
            }
        }
    }

    public static boolean isTheParrotAvailable(String parrotName) {
        boolean isAvailable = false;
        for (Parrot parrot : arrParrot) {
            if (parrot.getName().equals(parrotName)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }
}
