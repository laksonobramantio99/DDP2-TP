// Laksono Bramantio
// 1706984650

package animal;

import cage.Cage;
import java.util.ArrayList;
import java.util.Scanner;

public class Lion extends Animal {
    private static Scanner input = new Scanner(System.in);

    private String name;
    private int length;

    private static final String cageLoc = "outdoor";
    private static ArrayList<Lion> arrLion = new ArrayList<>();
    private static ArrayList<Cage>[] preArrangeLionArr;
    private static ArrayList<Cage>[] postArrangeLionArr;

    public Lion(String name, int length) {
        super(name, length);
    }

    public static ArrayList<Lion> getArrLion() {
        return arrLion;
    }

    public static String getCageLoc() {
        return cageLoc;
    }

    public static ArrayList<Cage>[] getPreArrangeLionArr() {
        return preArrangeLionArr;
    }

    public static void setPreArrangeLionArr(ArrayList<Cage>[] preArrangeLionArr) {
        Lion.preArrangeLionArr = preArrangeLionArr;
    }

    public static ArrayList<Cage>[] getPostArrangeLionArr() {
        return postArrangeLionArr;
    }

    public static void setPostArrangeLionArr(ArrayList<Cage>[] postArrangeLionArr) {
        Lion.postArrangeLionArr = postArrangeLionArr;
    }

    public static void visitLion() {
        System.out.print("Mention the name of lion you want to visit: ");
        String lionName = input.nextLine();

        if (!isTheLionAvailable(lionName)) {
            System.out.println("There is no lion with that name! Back to the office!\n");
        } else {
            System.out.println("You are visiting " + lionName + " (lion) now, what would you like to do?\n" +
                    "1: See it hunting 2: Brush the mane 3: Disturb it");
            String command = input.nextLine();

            if (command.equals("1")) { //See it hunting
                System.out.println("Lion is hunting..\n" +
                        lionName + " makes a voice: err...!\n" +
                        "Back to the office!\n");
            } else if (command.equals("2")) { //Brush the mane
                System.out.println("Clean the lion’s mane..\n" +
                        lionName + " makes a voice: Hauhhmm!\n" +
                        "Back to the office!\n");
            } else if (command.equals("3")) { //Disturb it
                System.out.println("Simba makes a voice: HAUHHMM!\n" +
                        "Back to the office!\n");
            } else {
                System.out.println("You do nothing...\n" +
                        "Back to the office!\n");
            }
        }
    }

    public static boolean isTheLionAvailable(String lionName) {
        boolean isAvailable = false;
        for (Lion lion : arrLion) {
            if (lion.getName().equals(lionName)) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }
}
