// Laksono Bramantio
// 1706984650

package animal;

public class Animal {
    private String name;
    private int length;

    public Animal(String name, int length) {
        this.name = name;
        this.length = length;
    }

    public String getName() {
        return name;
    }

    public int getLength() {
        return length;
    }
}
