import javax.swing.JFrame;

/**
 * Main Class
 * @author Laksono Bramantio
 *
 * Terinspirasi dari program:
 * https://codereview.stackexchange.com/questions/85833/basic-memory-match-game-in-java
 */
public class Game {
    public static void main(String[] args) {
        Board b = new Board();
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setVisible(true);
    }
}