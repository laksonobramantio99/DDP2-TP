import java.awt.*;
import java.awt.Container;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

import javax.swing.*;

@SuppressWarnings("serial")
/**
 * Class yang merepresentasikan keseluruhan body game
 *
 * @author Laksono Bramantio
 */
public class Board extends JFrame {

    // Panel cards
    private List<Card> cards = new ArrayList<>();
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private JPanel cardsPanel;

    // Panel buttons
    private JButton restart;
    private JButton exit;
    private JPanel midPanel;

    // Panel counter
    private int counter = 0;
    private JLabel labelCounter;
    private JPanel counterPanel;

    // Itself
    private Container pane = getContentPane();

    /**
     * Constructor of Board
     */
    public Board() {

        int pairs = 18;
        List<Integer> cardVals = new ArrayList<>();

        // Membuat acakan nomor
        for (int i = 0; i < pairs; i++) {
            cardVals.add(i);
            cardVals.add(i);
        }
        Collections.shuffle(cardVals);

        // Membuat card sesuai nomor dan menyimpan ke ArrayList
        for (int val : cardVals) {
            Card c = new Card(val);
            c.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    selectedCard = c;
                    doTurn();
                }
            });
            cards.add(c);
        }

        //============ SET UP TIMER DAN EKSEKUSI PENCOCOKAN ==========
        t = new Timer(750, new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                checkCards();
            }
        });
        t.setRepeats(false);


        //============ MEMBUAT PANEL 6x6 BUTTON ==============
        cardsPanel = new JPanel(new GridLayout(6, 6));
        for (Card c : cards) {
            cardsPanel.add(c);
        }

        //============== MEMBUAT PANEL TENGAH ===============
        // Set up the 'restart' button
        restart = new JButton("  Restart!  ");
        //playAgain.setSize(120, 20);
        restart.setFont(new Font("Arial", Font.BOLD, 18));
        restart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                restart();
            }
        });

        // Set up the 'exit' button
        exit = new JButton("     Exit!     ");
        exit.setFont(new Font("Arial", Font.BOLD, 18));
        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });

        // Set up Mid panel
        midPanel = new JPanel(new FlowLayout());
        midPanel.add(restart);
        midPanel.add(exit);


        //=========== MEMBUAT PANEL COUNTER LABEL ============
        // Set up the counter label
        labelCounter = new JLabel("Number of Tries: 0");
        labelCounter.setSize(120, 20);
        labelCounter.setFont(new Font("Arial", Font.BOLD, 20));

        // Set up counter Panel
        counterPanel = new JPanel(new FlowLayout());
        counterPanel.add(labelCounter);

        //========== MEMBUAT FRAME ITU SENDIRI =============
        // Set up the board itself
        pane.setLayout(new BorderLayout());
        setTitle("Memory Match Game [by Laksono B.]");

        pane.add(cardsPanel, BorderLayout.NORTH);
        pane.add(midPanel, BorderLayout.CENTER);
        pane.add(counterPanel, BorderLayout.SOUTH);
    }

    /**
     * Method untuk restart the game
     */
    public void restart() {
        for (Card c : cards) {
            c.setIcon(c.getCover());
            c.setEnabled(true);
            c.setMatched(false);
        }
        c1 = null;
        c2 = null;

        if (counter != 0) {
            counter = 0;
            labelCounter.setText("Number of Tries: 0");

            // Mengacak kartu kembali
            Collections.shuffle(cards);
            cardsPanel.removeAll();
            for (Card c : cards) {
                cardsPanel.add(c);
            }
        }
    }

    /**
     * Method ketika ada card yang di-klik
     */
    public void doTurn() {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.setIcon(c1.getImage());
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.setIcon(c2.getImage());
            t.start();
            counter++;
            labelCounter.setText("Number of Tries: " + counter);
        }
    }

    /**
     * Method untuk mengecek apakah ada
     * sama atau tidak
     */
    public void checkCards() {
        if (c1.getId() == c2.getId()) {//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()) {
                //JOptionPane.showMessageDialog(this, "You win this Game!");
                JOptionPane.showMessageDialog(this, "You won!",
                        "Congratulation", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            c1.setIcon(c1.getCover()); //'hides' the image
            c2.setIcon(c2.getCover());
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    /**
     * Method untuk mengecek kemenangan tiap turn
     *
     * @return true jika sudah menang
     * false jika belum menang
     */
    public boolean isGameWon() {
        for (Card c : this.cards) {
            if (c.isMatched() == false) {
                return false;
            }
        }
        return true;
    }
}