import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;

@SuppressWarnings("serial")
/**
 * Class yang merepresentasikan sebuah object
 * card/tombol untuk tiap card pada program
 *
 * @author Laksono Bramantio
 */
public class Card extends JButton {
    private int id;
    private boolean matched = false;
    private ImageIcon image;
    private ImageIcon cover;

    /**
     * Constructor
     * Menginisiasi sebuah kartu/tombol
     */
    public Card(int val) {
        try {
            this.id = val;

            // Back end image
            Image img = ImageIO.read(new File("D:\\code\\DDP2-TP\\assignment-4\\img\\" + val + ".png"));
            Image newImg = img.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
            this.image = new ImageIcon(newImg);

            // Cover image
            Image cover = ImageIO.read(new File("D:\\code\\DDP2-TP\\assignment-4\\img\\coverButton.png"));
            Image newCover = cover.getScaledInstance(120, 120, java.awt.Image.SCALE_SMOOTH);
            this.cover = new ImageIcon(newCover);

            this.setIcon(this.cover);
        } catch (Exception e) {
            System.out.println("Error: Cannot import the image");
        }
    }

    /**
     * Method getter ID
     */
    public int getId() {
        return this.id;
    }

    /**
     * Method setter match
     */
    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    /**
     * Method getter match
     */
    public boolean isMatched() {
        return this.matched;
    }

    /**
     * Method getter inside image
     */
    public ImageIcon getImage() {
        return image;
    }

    /**
     * Method getter cover image
     */
    public ImageIcon getCover() {
        return cover;
    }
}