// Nama : Laksono Bramantio
// NPM  : 1906984650

import java.util.Scanner;

public class A1Station {
    
    private static final double THRESHOLD = 250; // in kilograms
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        try {
            int jmlInput = Integer.parseInt(input.nextLine());
            
            TrainCar[] arrayCat = new TrainCar[jmlInput];
            boolean status = true;
            int count = 0;
            
            // Meminta input sesuai angka inputan pertama
            for (int i = 0; i < jmlInput; i++) {
                
                String[] temp = input.nextLine().split(",");
                double weightCat = Double.parseDouble(temp[1]);
                double lengthCat = Double.parseDouble(temp[2]);
                
                WildCat catObject = new WildCat(temp[0], weightCat, lengthCat);
                
                // Gerbong paling belakang
                if (status) {
                    TrainCar carCat = new TrainCar(catObject);
                    arrayCat[i] = carCat;
                    status = false;
                    count += 1;
                } else { // Gerbong setelahnya
                    TrainCar carCat = new TrainCar(catObject, arrayCat[i - 1]);
                    arrayCat[i] = carCat;
                    count += 1;
                }
                
                // Jika berat total > 250
                if (arrayCat[i].computeTotalWeight() >= THRESHOLD) {
                    printCarCat(arrayCat, i, count);
                    status = true;
                    count = 0;
                } else if (i == (jmlInput - 1)) { // Jika sudah di akhir input
                    printCarCat(arrayCat, i, count);
                }
            }
        } catch (Exception e) {
            System.out.println("Invalid input");
            System.exit(0);
        }
    }
    
    // Method print tiap melebihi 250 atau di akhir input
    public static void printCarCat(TrainCar[] arrayCat, int i, int count) {
        double avgBmi = (double) (arrayCat[i].computeTotalMassIndex() / count);
        
        System.out.println("The train departs to Javari Park");
        System.out.print("[LOCO]<");
        arrayCat[i].printCar();
        System.out.printf("Average mass index of all cats: %.2f\n", avgBmi);
        
        if (avgBmi < 18.5) {
            System.out.println("In average, the cats in the train are *underweight*");
        } else if (avgBmi < 25) {
            System.out.println("In average, the cats in the train are *normal*");
        } else if (avgBmi < 30) {
            System.out.println("In average, the cats in the train are *overweight*");
        } else {
            System.out.println("In average, the cats in the train are *obese*");
        }
    }
}
