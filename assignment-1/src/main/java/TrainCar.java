public class TrainCar {

    public static final double EMPTY_WEIGHT = 20; // In kilograms
    private WildCat cat;
    private TrainCar next;

    public TrainCar(WildCat cat) {
        this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
        this.cat = cat;
        this.next = next;
    }

    // Method Hitung Total Weight
    public double computeTotalWeight() {
        if (next == null) {
            return EMPTY_WEIGHT + cat.weight;
        }
        return EMPTY_WEIGHT + cat.weight + next.computeTotalWeight();
    }

    // Method Hitung Total Mass Index
    public double computeTotalMassIndex() {
        if (next == null) {
            return cat.computeMassIndex();
        }
        return cat.computeMassIndex() + next.computeTotalMassIndex();
    }

    // Method Print Car
    public void printCar() {
        if (next == null) {
            System.out.println("--(" + cat.name + ")");
        } else {
            System.out.print("--(" + cat.name + ")");
            next.printCar();
        }
    }
}


