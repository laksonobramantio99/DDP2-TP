package javari.animal;

public class Mammal extends Animal {

    private boolean isPregnant = false;

    public Mammal(Integer id, String type, String name, Gender gender, double length,
                  double weight, String ispregnant, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (ispregnant.equals("pregnant")) {
            this.isPregnant = true;
        }
    }

    @Override
    public boolean specificCondition() {
        if (getType().equals("Lion")) {
            if (getGender() == Gender.MALE && !isPregnant) {
                return true;
            }
            return false;
        } else {
            if (!isPregnant) {
                return true;
            }
            return false;
        }
    }
}
