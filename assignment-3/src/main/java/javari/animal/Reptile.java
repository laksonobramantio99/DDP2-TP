package javari.animal;

public class Reptile extends Animal {

    private boolean isTame = false;

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                   double weight, String istame, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (istame.equals("tame")) {
            this.isTame = true;
        }
    }

    @Override
    public boolean specificCondition() {
        if (isTame) {
            return true;
        }
        return false;
    }
}
