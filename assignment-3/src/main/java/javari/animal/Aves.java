package javari.animal;

public class Aves extends Animal {

    private boolean isLayingEggs = false;

    public Aves(Integer id, String type, String name, Gender gender, double length,
                double weight, String islayingeggs, Condition condition) {
        super(id, type, name, gender, length, weight, condition);
        if (islayingeggs.equals("laying eggs")) {
            this.isLayingEggs = true;
        }
    }

    @Override
    public boolean specificCondition() {
        if (!isLayingEggs) {
            return true;
        }
        return false;
    }
}
