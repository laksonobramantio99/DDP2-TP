package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class RecordsReader extends CsvReader {

    public RecordsReader(Path file) throws IOException {
        super(file);
    }

    @Override
    public long countValidRecords() {
        long valid = 0;
        for (String record : lines) {
            String[] data = record.split(COMMA);
            if (data.length == 8) {
                valid += 1;
            }
        }
        return valid;
    }

    @Override
    public long countInvalidRecords() {
        long invalid = 0;
        for (String record : lines) {
            String[] data = record.split(COMMA);
            if (data.length != 8) {
                invalid += 1;
            }
        }
        return invalid;
    }
}
