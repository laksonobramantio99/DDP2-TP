package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class SectionsReader extends CsvReader {

    private List<String> sections = new ArrayList<String>();

    public SectionsReader(Path file) throws IOException {
        super(file);
    }

    public List<String> getSections() {
        return sections;
    }

    @Override
    public long countValidRecords() {
        for (String record : lines) {
            String[] data = record.split(COMMA);
            try {
                for (String section : sections) {
                    if (data[2].equals(section)) {
                        throw new Exception();
                    }
                }
                sections.add(data[2]);
            } catch (Exception e) {
                continue;
            }
        }
        return (long) sections.size();
    }

    @Override
    public long countInvalidRecords() {
        long invalid = 0;
        for (String record : lines) {
            String[] data = record.split(COMMA);
            if (data.length != 3) {
                invalid += 1;
            }
        }
        return invalid;
    }
}
