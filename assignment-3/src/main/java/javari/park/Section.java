package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Section {

    private String name;
    private List<String> animal = new ArrayList<String>();

    public Section(String name) {
        this.name = name;
    }

    public void addAnimal(String type) {
        animal.add(type);
    }

    public List<String> getAnimal() {
        return animal;
    }

    public String getName() {
        return name;
    }
}
