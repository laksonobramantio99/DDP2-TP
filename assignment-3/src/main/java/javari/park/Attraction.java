package javari.park;

import javari.animal.Animal;

import java.util.ArrayList;
import java.util.List;

public class Attraction implements SelectedAttraction {

    private String name;
    private String type;
    private List<Animal> performers = new ArrayList<Animal>();

    public Attraction(String type, String name) {
        this.type = type;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public List<Animal> getPerformers() {
        return performers;
    }

    @Override
    public boolean addPerformer(Animal performer) {
        if (performer.getType().equals(type) && performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }
}
