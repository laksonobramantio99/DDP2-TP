package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

    private int id;
    private String name;
    private List<SelectedAttraction> selectedAttraction = new ArrayList<SelectedAttraction>();

    public Visitor(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int getRegistrationId() {
        return id;
    }

    @Override
    public String getVisitorName() {
        return name;
    }

    @Override
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAttraction;
    }

    @Override
    public String setVisitorName(String name) {
        String oldName = this.name;
        this.name = name;
        return oldName + " changed to" + name;
    }

    @Override
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        if (selected == null) {
            return false;
        }
        selectedAttraction.add(selected);
        return true;
    }
}
